@isTest
public class TestLeadConvertToTask {
    
    @testSetup
     static void setup(){
        
        Lead objLead = new Lead();
        objLead.FirstName = 'Test';
        objLead.LastName = 'Lead';
        objLead.Company = 'Silverline';
         
        insert objLead;
        
        Database.LeadConvert objLeadds = new database.LeadConvert();  
        objLeadds.setLeadId( objLead.Id );    
        objLeadds.setConvertedStatus( 'Closed - Converted' );  
        Database.LeadConvertResult leadConvertedResult = Database.convertLead(objLeadds);
         
         
        Lead objLead1 = new Lead();
        objLead1.FirstName = 'Tests';
        objLead1.LastName = 'Leads';
        objLead1.Company = 'SilverlineCRM';
         
        insert objLead1;
        
        Database.LeadConvert objLeads = new database.LeadConvert();  
        objLeads.setLeadId( objLead1.Id );    
        objLeads.setConvertedStatus( 'Closed - Converted' );  
        Database.LeadConvertResult leadConvertedResultt = Database.convertLead(objLeads); 
            
        Lead objNewLead = [SELECT Id, OwnerId, IsConverted, ConvertedContactId FROM Lead WHERE FirstName = 'Tests' LIMIT 1];
        Task objTask = new Task();
        objTask.WhoId = objNewLead.ConvertedContactId;
        objTask.OwnerId = objNewLead.OwnerId;
        objTask.Subject = 'Please reach out to the Contact to understand next steps';
         
        insert objTask;
    }
    @isTest static void test() {
        Test.startTest();
        LeadConvertToTask Lc = new LeadConvertToTask();
        Id batchId = Database.executeBatch(Lc);
        Test.stopTest(); 
        
       Lead objLead = new Lead();
        objLead.FirstName = 'Test';
        objLead.LastName = 'Lead';
        objLead.Company = 'Silverline';
       
        
        List<Lead> lstLeads = ([SELECT OwnerId,ConvertedContactId,ConvertedAccountId FROM Lead WHERE Id =: objLead.Id LIMIT 3]);
        List<Task> lstTasks = ([SELECT Id,OwnerId,Subject,WhoId,WhatId FROM Task LIMIT 5]);
        
        if(!lstLeads.isEmpty()){
            System.assertEquals(5,lstTasks.size());
            System.assertEquals(3,lstLeads.size());
        }
    }

}