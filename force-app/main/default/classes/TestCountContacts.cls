@isTest
private class TestCountContacts {
    
    @testSetup
    static void setup()
    {
        List<Account> lstAcc = new List<Account>();
        Account Acc = new Account(Name='TestAccount');
        lstAcc.add(Acc);
        insert lstAcc;
        Account objId = [SELECT Id FROM Account WHERE Name = 'TestAccount'];
        
        Id directRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Direct').getRecordTypeId();
        Id indirectRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Indirect').getRecordTypeId();
        
        List<Contact> lstCon = new List<Contact>();
        for(Integer i = 0; i < = 1; i++)
        {
            Contact objCon = new Contact();
            objCon.lastName = 'Test'+i;
            objCon.RecordTypeId = directRecordTypeId;
            objCon.AccountId = objId.Id;
            lstCon.add(objCon);
        }
        for(Integer j = 0; j < = 1; j++) 
        {
            Contact objCon = new Contact();
            objCon.lastName = 'Test'+j;
            objCon.RecordTypeId = indirectRecordTypeId;
            objCon.AccountId = objId.Id;
            lstCon.add(objCon);   
        }
        insert lstCon; 
    }
    @isTest static void test() 
    {
        Test.startTest();
        CountContacts Tc = new CountContacts();
        Id batchId = Database.executeBatch(Tc);
        Test.stopTest(); 
        List<Account> lstAccounts = [SELECT Id,Name,Direct_Contacts__c,Indirect_Contacts__c FROM Account];
        for(Account objAcc : lstAccounts) 
        {
            System.assertEquals(2, objAcc.Direct_Contacts__c);
            System.assertEquals(2, objAcc.Indirect_Contacts__c);
        }
            
        }
}