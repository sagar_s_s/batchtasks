public class LeadConvertToTask implements Database.Batchable<sObject> {

     public Database.QueryLocator start(Database.BatchableContext sg) 
    {
        return Database.getQueryLocator([SELECT Id,OwnerId,FirstName,LastName,Company,Status,LeadSource,IsConverted,ConvertedDate,CreatedDate,ConvertedAccountId,ConvertedContactId FROM Lead WHERE CALENDAR_YEAR(CreatedDate) = 2021]);
    }

    public void execute(Database.BatchableContext ss, List<Lead> lstLeads) {
        //Hello Sagar

        List<Task> lstTasks = new List<Task>();
        Set<Id> setContactIds = new Set<Id>();
        //hello morning


        //Map<Id,Task> mapToGetWhoId = new Map<Id,Task>([SELECT Id,Subject,WhoId FROM Task WHERE WhoId IN : setContactIds]);
        set<Id> setContactIdsOfTask = new Set<Id>();
        
        for(Lead objLead : lstLeads) {
            setContactIds.add(objLead.ConvertedContactId);
             System.debug('setContactIds' +setContactIds);
        }
        for(Task objTask : [SELECT Id,Subject,WhoId FROM Task WHERE WhoId IN : setContactIds AND Subject = 'Please reach out to the Contact to understand next steps']) {
            setContactIdsOfTask.add(objTask.WhoId);
            System.debug('setContactIdsOfTask' +setContactIdsOfTask);
        }
        for(Lead objLeadd : lstLeads) {
            if(!setContactIdsOfTask.contains(objLeadd.ConvertedContactId))  {
                Task objTaskNew = new Task();
                objTaskNew.OwnerId = objLeadd.OwnerId;
                objTaskNew.Type = 'phone';
                objTaskNew.Subject = 'Please reach out to the Contact to understand next steps';
                objTaskNew.Status = 'Completed';
                objTaskNew.ActivityDate = objLeadd.ConvertedDate + 30;
                objTaskNew.Description = 'Successfully Done';
                objTaskNew.WhatId = objLeadd.ConvertedAccountId;
                objTaskNew.WhoId = objLeadd.ConvertedContactId;
                lstTasks.add(objTaskNew);
                System.debug('lstTasks' +lstTasks);
            }
    }
    if(!lstTasks.isEmpty()) {
        insert lstTasks;
    }
}
    public void finish(Database.BatchableContext sg) {

        System.debug('Donee');
    }

}