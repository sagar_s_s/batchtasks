public class CountContacts implements Database.Batchable<sObject> 
{
    public Database.QueryLocator start(Database.BatchableContext ss) 
    {
        return Database.getQueryLocator([SELECT Id,Name,AccountId,RecordTypeId FROM Contact WHERE RecordTypeId != Null]);
    }
    
    public void execute(Database.BatchableContext ss, List<Contact> lstContacts) 
    {
        
        Map<Id, Integer> mapToDirectCount = new Map<Id, Integer>();
        Map<Id, Integer> mapToIndirectCount = new Map<Id, Integer>();
        //Map<Id, Account> mapAccountToAccount = new Map<Id, Account>();
        Id directRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Direct').getRecordTypeId();
        Id indirectRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Indirect').getRecordTypeId();
        
        //Set<Id> accIds = new Set<Id>();
        
        for(Contact objCon : lstContacts) 
        { 
            if(objCon.RecordTypeId == directRecordTypeId)
            {
                if(mapToDirectCount.containsKey(objCon.AccountId))
                {
                    System.debug('Direct contains');
                     mapToDirectCount.put(objCon.AccountId, mapToDirectCount.get(objCon.AccountId)+1);
                    
                }
                else
                {
                    System.debug('Direct not contains');
                   mapToDirectCount.put(objCon.AccountId,1);
                }
            } 
            if(objCon.RecordTypeId == indirectRecordTypeId)
            {
                if(!mapToIndirectCount.containsKey(objCon.AccountId))
                {
                    mapToIndirectCount.put(objCon.AccountId,1);
                }
                else
                {
                    mapToIndirectCount.put(objCon.AccountId, mapToIndirectCount.get(objCon.AccountId) +1);
                }
            }
            
            //mapAccountToAccount(objCon.AccountId,);
            
        }
        Set<Id> accountIds = new Set<Id>();
        accountIds.addAll(mapToDirectCount.keySet());
        accountIds.addAll(mapToIndirectCount.keySet());
        List<Account> lstAccountToUpdate = new List<Account>();

        for(Id accId : accountIds){
            Account objAcc = new Account(Id = accId);
            objAcc.Direct_Contacts__c = mapToDirectCount.containsKey(accId) ? mapToDirectCount.get(accId) : 0;
            objAcc.Indirect_Contacts__c = mapToIndirectCount.containsKey(accId) ? mapToIndirectCount.get(accId) : 0;
            lstAccountToUpdate.add(objAcc);
        }

        if(!lstAccountToUpdate.isEmpty()){
            update lstAccountToUpdate;
        }
    }
    public void finish(Database.BatchableContext ss) {
        System.debug('done');
    }
}